#for Standard Deviation Part:
import statistics

#list of vowels:
vowels=['a', 'e', 'i', 'o', 'u']
constants=[]

#If digit entered it will give another chance
x=0
while x ==0:
    user_input = input("Enter a sentence with 3 words. \nIf you are unsure what to do type help: ")

    if user_input.isdigit(): # "if user_input is a digit:"
        print('You entered a combination of numbers!\n')
        continue #circles back so you get another chance

    else: #This is for if the user_input is a string:

        # If user is unsure what to do it will print this:
        if user_input == 'help':
            print('When you enter a sentence with 3 words, this code will count the number of vowels in each word. \nThen it will sort each word from greatest to smallest!')
    #CORE CODE:
        else:
            # makes the sentence into a list so its easy to work with:
            user_input.lower()  # makes all characters lowercase
            user_list = user_input.split()  # makes sentence into a list

            # empty list for later:
            count_list = []

            #This checks for if there is a vowel in each word
            for i in user_list: #for each word in the list
                count = 0  # have to put it here bc we have to reset it for every word

                for x in i:  # for each letter in each word
                    if (x in vowels): #if letter is in the vowels list:
                        count += 1  # adds one to the count

            #for each word it will add a new number to our count_list
                count_list.append(count)

                #prints how many vowels are in each word
                print("There is " + str(count) + " vowels in " + (str(i)))

            
        #My sorting technique:
            #Bubble sort:
            def sort(count_list):
                for i in range(len(count_list) - 1, 0, -1):
                    for j in range(i):
                        if count_list[j] > count_list[j + 1]:  # bellow will swap the numbers if needed to swap.
                            t = count_list[j]  # t is a temp. variable
                            count_list[j] = count_list[j + 1]
                            count_list[j + 1] = t

                        #This part makes the user_list into the same order as the count_list
                            t = user_list[j]  # temp_word is a temp. variable
                            user_list[j] = user_list[j + 1]
                            user_list[j + 1] = t

            #sortrs the count_list
            sort(count_list)
            #Reverses the order so it becomes greatest to least 
            user_list.reverse()

            #prints the sentance in correct order:
            print(user_list)

            # For standard deviation:
            print("Standard Deviation : % s " % (statistics.stdev(count_list)))
